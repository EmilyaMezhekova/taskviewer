from django.apps import AppConfig


class TaskviewerConfig(AppConfig):
    name = 'taskviewer'
