from django.db import models
from django.utils import timezone


# id создается автоматически и используется как первичый ключ
class Tasks(models.Model):
    class Meta:
        ordering = ['-id']
    create_time = models.DateTimeField(verbose_name="Время создания задачи", blank=False, auto_now_add=True)
    finish_time = models.DateTimeField(verbose_name="Время завершения задачи", blank=True, null=True, )

# Create your models here.
