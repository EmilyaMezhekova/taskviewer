from django.shortcuts import render
from taskviewer.forms import *
from taskviewer.models import *
import threading
from django.http import HttpResponse
from multiprocessing.pool import ThreadPool
from django.utils import timezone
from test import func
import json

lock = threading.Lock()
current_task = None
delta = []

def add_task(in_id):
    global current_task, delta
    lock.acquire()
    current_task = in_id
    func()
    lock.release()
    current_task = None
    tmp = Tasks.objects.get(id=in_id)
    tmp.finish_time = timezone.now()
    tmp.save()
    delta.append([in_id, tmp.finish_time.strftime("%B %d, %Y, %I:%M%p")])



def MainPage(request):
    if request.method == 'POST':
        form = CreateTask(request.POST)
        form.save()
        pool = ThreadPool(processes=1)
        pool.apply_async(add_task, args={Tasks.objects.latest('id').id})
    return render(request, 'index.html')


def ListPage(request):
    context = {}
    if request.method == 'POST':
        global current_task
        global delta
        finished = []
        if delta:
            finished = json.dumps(delta)
            del delta[:]
        return HttpResponse(
            json.dumps({
                "current": current_task,
                "finished": finished
            }),
            content_type="application/json"
        )
    if request.method == 'GET':
        context['tasks'] = Tasks.objects.all()

    return render(request, 'list.html', context)

# Create your views here.
