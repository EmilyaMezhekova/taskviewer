from django.contrib import admin
from taskviewer.models import *

class TaskAdmin(admin.ModelAdmin):
    # Keep the fields readonly
    readonly_fields = ['id', 'create_time']

    # The fields in the order you want them
    fieldsets = (
        (None, {
            'fields': ('id', 'create_time', 'finish_time')
        }),
    )

admin.site.register(Tasks, TaskAdmin)

