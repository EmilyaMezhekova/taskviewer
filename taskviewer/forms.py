from django import forms
from taskviewer.models import *

class CreateTask(forms.ModelForm):
    class Meta:
        model = Tasks
        fields = ['finish_time']