$(document).ready(function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');
    $('#btn').click(function () {

        $.ajax({
            method: "POST",
            url: "/",
            data: {
                csrfmiddlewaretoken: csrftoken
            },
            //    success: function(data) {
            //   alert("Задача добавлена!");
            // },
        })
    });
    var check_tasks = setInterval(function () {
        $.ajax({
            url: "list/",
            type: 'POST',
            data: {
                csrfmiddlewaretoken: csrftoken
            },
            success: function (json) {
                if (json.current) {
                    var td = $('td:first-child:contains(' + json.current + ')');
                    td.next('td').text('Выполняется')
                }
                if (json.finished.length>0){
                   var parse = $.parseJSON(json.finished)
                $('td:first-child').map(function () {
                    var self = this;
                    parse.map(function (value) {
                         if (Number($(self).text())== value[0]) {
                             $(self).css("background-color", "None")
                             $(self).next('td').text('Завершена');
                             $(self).next('td').next('td').next('td').text(value[1])
                         }
                    })
                })}
             }
        });
    }, 6000)

    $('#link').click(function () {
        $.ajax({
            url: "list/",

            success: function (html) {
                $("#content").html(html)
                history.pushState(html, "page 2", "/list/");
            }
        });
    });
    $('#main').click(function () {
        $.ajax({
            url: "/",
            cache: false,
            success: function (html) {
                $("#content").html(html);
                history.pushState(html, "page 1", "/");
            }
        });
    });

});